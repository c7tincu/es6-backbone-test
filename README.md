es6-backbone-test
=================

Step 1
------

Install [jspm](http://jspm.io/) globally —This isn't ideal, but for this test, it’ll do just fine:

```
sudo npm i jspm -g
```

Step 2
------

Run:

```
jspm init
```

Make sure you specify `src` as the project code folder, at the fourth confirmation prompt. Otherwise, go with the defaults:

```
Package.json file does not exist, create it? [yes]:
Would you like jspm to prefix the jspm package.json properties under jspm? [yes]:
Enter server baseURL (public folder path) [./]:
Enter project code folder [./]: src
Enter jspm packages folder [./jspm_packages]:
Enter config file path [./config.js]:
Configuration file config.js doesn't exist, create it? [yes]:
Enter client baseURL (public folder URL) [/]:
Which ES6 transpiler would you like to use, Traceur or Babel? [traceur]:
```

Step 3
------

Install Backbone & friends:

```
jspm install npm:jquery npm:lodash npm:backbone
```

Step 4
------

We'll open `index.html` as a file, so we need to make sure that:

1. We open `config.js` and set `baseURL:` to `'.'`;
2. We run our browser of choice with the “file access from files” option enabled:

  * For Chrome: Pass `--allow-file-access-from-files` as argument to the Chrome executable, when launching the browser;
  * For Firefox: “Navigate” to `about:config`, enter `security.fileuri.strict_origin_policy` in the filter box and toggle the option to `false`.

Step 5
------

Open `index.html` (as a file) in your browser, checkout the console output, and rejoyce!
