import Backbone from 'backbone';

class TestModel extends Backbone.Model {

  url() {
    return 'test.json';
  }

  constructor() {
    super();
    this.fetch()
      .done(() => {
        console.log('foo: ', this.get('foo'), '.');
        console.log(new Date().toISOString());
      })
      .fail(() => {
        console.log('fail!');
        console.log(new Date().toISOString());
      });
  }

}

export default TestModel;
